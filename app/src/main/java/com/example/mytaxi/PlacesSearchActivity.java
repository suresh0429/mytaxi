package com.example.mytaxi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.mytaxi.Retrofit.MyApi;
import com.google.android.gms.location.places.Place;

import com.shishank.autocompletelocationview.LocationAutoCompleteView;
import com.shishank.autocompletelocationview.interfaces.OnQueryCompleteListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.CompositeDisposable;

public class PlacesSearchActivity extends AppCompatActivity implements OnQueryCompleteListener {
    private static final String TAG = "PlacesSearchActivity";
    String stationName;
    MyApi api;
    CompositeDisposable compositeDisposable = new CompositeDisposable();

    /*@BindView(R.id.edit_query)
    EditText editQuery;*/
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    /*@BindView(R.id.rcProduct)
    RecyclerView rcProduct;*/
    @BindView(R.id.progressBar)
    ProgressBar progressBar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_places_search);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if (getIntent() != null) {
            stationName = getIntent().getStringExtra("Title");
            Log.e(TAG, "onCreate: " + stationName);
        }


        LocationAutoCompleteView autoCompleteLocation = findViewById(R.id.autocomplete_view);
        autoCompleteLocation.setCompletionHint(stationName);
        autoCompleteLocation.setOnQueryCompleteListener(this);

       /* Retrofit retrofit = RetrofitClient.getInstance();
        api = retrofit.create(MyApi.class);

        editQuery.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {

                String newText = s.toString();
                fetchDataList(newText);


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });*/




    }

   /* private void fetchDataList(String text) {
        progressBar.setVisibility(View.VISIBLE);
        compositeDisposable.add(api.getAllPlaces(getResources().getString(R.string.google_maps_key), "country:in", text)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(posts -> {

                    progressBar.setVisibility(View.GONE);

                    Log.d(TAG, "accept: " + posts.getPredictions().size());

                    adapter = new SearchAdapter(PlacesSearchActivity.this, posts.getPredictions());
                    rcProduct.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }));
    }

    @Override
    protected void onStop() {
        super.onStop();
        compositeDisposable.clear();
    }*/

   @Override
     public void onTextClear() {

    }

    @Override
    public void onPlaceSelected(Place place) {
        /*map.addMarker(new MarkerOptions().position(selectedPlace.getLatLng()));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(selectedPlace.getLatLng(), 16));*/

        Intent returnIntent = new Intent();
        returnIntent.putExtra("result",place.getAddress());
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
    }



    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
