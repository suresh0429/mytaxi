package com.example.mytaxi.Retrofit;


import com.example.mytaxi.Response.GoogleplacesResponse;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface MyApi {
    @GET("json")
    Observable<GoogleplacesResponse> getAllPlaces(@Query("key") String Key,
                                                @Query("components") String components,
                                                @Query("input") String input);

    /*@FormUrlEncoded
    @POST("user_login")
    Observable<LoginResponse> userLogin(@Field("mobile") String mobile, @Field("password") String password);
*/
}
