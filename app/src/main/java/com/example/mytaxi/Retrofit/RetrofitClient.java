package com.example.mytaxi.Retrofit;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static  Retrofit ourInstance;

   public static Retrofit getInstance() {
       if (ourInstance == null)
           ourInstance = new Retrofit.Builder()
                  // .baseUrl("https://jsonplaceholder.typicode.com/")
                   .baseUrl("https://maps.googleapis.com/maps/api/place/autocomplete/")
                   .addConverterFactory(GsonConverterFactory.create())
                   .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                   .build();
        return ourInstance;
    }

    private RetrofitClient() {
    }
}
