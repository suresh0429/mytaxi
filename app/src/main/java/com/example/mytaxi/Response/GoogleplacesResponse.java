package com.example.mytaxi.Response;

import java.util.List;

public class GoogleplacesResponse {


    /**
     * predictions : [{"description":"Kukatpally, Hyderabad, Telangana, India","id":"24bb1d4176a23c3bff82bad40365406c92acbfa8","matched_substrings":[{"length":3,"offset":0}],"place_id":"ChIJPfRiAeyRyzsRSM9YQ_7GiDI","reference":"ChIJPfRiAeyRyzsRSM9YQ_7GiDI","structured_formatting":{"main_text":"Kukatpally","main_text_matched_substrings":[{"length":3,"offset":0}],"secondary_text":"Hyderabad, Telangana, India"},"terms":[{"offset":0,"value":"Kukatpally"},{"offset":12,"value":"Hyderabad"},{"offset":23,"value":"Telangana"},{"offset":34,"value":"India"}],"types":["sublocality_level_1","sublocality","political","geocode"]},{"description":"Kukke Shri Subrahmanya Temple, Mardala - Sullia, Subrahamanya, Karnataka, India","id":"37fe20fff065f831f95679bbd787962c8530dc5e","matched_substrings":[{"length":3,"offset":0}],"place_id":"ChIJcS7UXR1uqDsRENF-BOtYAME","reference":"ChIJcS7UXR1uqDsRENF-BOtYAME","structured_formatting":{"main_text":"Kukke Shri Subrahmanya Temple","main_text_matched_substrings":[{"length":3,"offset":0}],"secondary_text":"Mardala - Sullia, Subrahamanya, Karnataka, India"},"terms":[{"offset":0,"value":"Kukke Shri Subrahmanya Temple"},{"offset":31,"value":"Mardala - Sullia"},{"offset":49,"value":"Subrahamanya"},{"offset":63,"value":"Karnataka"},{"offset":74,"value":"India"}],"types":["hindu_temple","place_of_worship","point_of_interest","establishment"]},{"description":"Kukke Subrahmanya, Subrahmanya, Karnataka, India","id":"aef50646612cb771d5f190d297702b0e78d55f17","matched_substrings":[{"length":3,"offset":0}],"place_id":"ChIJz4lj8QzkpDsRQ41MFq95iuM","reference":"ChIJz4lj8QzkpDsRQ41MFq95iuM","structured_formatting":{"main_text":"Kukke Subrahmanya","main_text_matched_substrings":[{"length":3,"offset":0}],"secondary_text":"Subrahmanya, Karnataka, India"},"terms":[{"offset":0,"value":"Kukke Subrahmanya"},{"offset":19,"value":"Subrahmanya"},{"offset":32,"value":"Karnataka"},{"offset":43,"value":"India"}],"types":["point_of_interest","establishment"]},{"description":"Kukas, Rajasthan, India","id":"2188689a39bfbf063b66d08ccb2460690d3c74e9","matched_substrings":[{"length":3,"offset":0}],"place_id":"ChIJNV5yv--vbTkR304umhKl9mU","reference":"ChIJNV5yv--vbTkR304umhKl9mU","structured_formatting":{"main_text":"Kukas","main_text_matched_substrings":[{"length":3,"offset":0}],"secondary_text":"Rajasthan, India"},"terms":[{"offset":0,"value":"Kukas"},{"offset":7,"value":"Rajasthan"},{"offset":18,"value":"India"}],"types":["locality","political","geocode"]},{"description":"Kukatpally Housing Board Colony, Kukatpally, Hyderabad, Telangana, India","id":"79c701768d669e222c8ead2fc618b277af3916e2","matched_substrings":[{"length":3,"offset":0}],"place_id":"ChIJwykfvrWRyzsRKCNCPtJxr_U","reference":"ChIJwykfvrWRyzsRKCNCPtJxr_U","structured_formatting":{"main_text":"Kukatpally Housing Board Colony, Kukatpally","main_text_matched_substrings":[{"length":3,"offset":0}],"secondary_text":"Hyderabad, Telangana, India"},"terms":[{"offset":0,"value":"Kukatpally Housing Board Colony"},{"offset":33,"value":"Kukatpally"},{"offset":45,"value":"Hyderabad"},{"offset":56,"value":"Telangana"},{"offset":67,"value":"India"}],"types":["sublocality_level_2","sublocality","political","geocode"]}]
     * status : OK
     */

    private String status;
    private List<PredictionsBean> predictions;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<PredictionsBean> getPredictions() {
        return predictions;
    }

    public void setPredictions(List<PredictionsBean> predictions) {
        this.predictions = predictions;
    }

    public static class PredictionsBean {
        /**
         * description : Kukatpally, Hyderabad, Telangana, India
         * id : 24bb1d4176a23c3bff82bad40365406c92acbfa8
         * matched_substrings : [{"length":3,"offset":0}]
         * place_id : ChIJPfRiAeyRyzsRSM9YQ_7GiDI
         * reference : ChIJPfRiAeyRyzsRSM9YQ_7GiDI
         * structured_formatting : {"main_text":"Kukatpally","main_text_matched_substrings":[{"length":3,"offset":0}],"secondary_text":"Hyderabad, Telangana, India"}
         * terms : [{"offset":0,"value":"Kukatpally"},{"offset":12,"value":"Hyderabad"},{"offset":23,"value":"Telangana"},{"offset":34,"value":"India"}]
         * types : ["sublocality_level_1","sublocality","political","geocode"]
         */

        private String description;
        private String id;
        private String place_id;
        private String reference;
        private StructuredFormattingBean structured_formatting;
        private List<MatchedSubstringsBean> matched_substrings;
        private List<TermsBean> terms;
        private List<String> types;

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPlace_id() {
            return place_id;
        }

        public void setPlace_id(String place_id) {
            this.place_id = place_id;
        }

        public String getReference() {
            return reference;
        }

        public void setReference(String reference) {
            this.reference = reference;
        }

        public StructuredFormattingBean getStructured_formatting() {
            return structured_formatting;
        }

        public void setStructured_formatting(StructuredFormattingBean structured_formatting) {
            this.structured_formatting = structured_formatting;
        }

        public List<MatchedSubstringsBean> getMatched_substrings() {
            return matched_substrings;
        }

        public void setMatched_substrings(List<MatchedSubstringsBean> matched_substrings) {
            this.matched_substrings = matched_substrings;
        }

        public List<TermsBean> getTerms() {
            return terms;
        }

        public void setTerms(List<TermsBean> terms) {
            this.terms = terms;
        }

        public List<String> getTypes() {
            return types;
        }

        public void setTypes(List<String> types) {
            this.types = types;
        }

        public static class StructuredFormattingBean {
            /**
             * main_text : Kukatpally
             * main_text_matched_substrings : [{"length":3,"offset":0}]
             * secondary_text : Hyderabad, Telangana, India
             */

            private String main_text;
            private String secondary_text;
            private List<MainTextMatchedSubstringsBean> main_text_matched_substrings;

            public String getMain_text() {
                return main_text;
            }

            public void setMain_text(String main_text) {
                this.main_text = main_text;
            }

            public String getSecondary_text() {
                return secondary_text;
            }

            public void setSecondary_text(String secondary_text) {
                this.secondary_text = secondary_text;
            }

            public List<MainTextMatchedSubstringsBean> getMain_text_matched_substrings() {
                return main_text_matched_substrings;
            }

            public void setMain_text_matched_substrings(List<MainTextMatchedSubstringsBean> main_text_matched_substrings) {
                this.main_text_matched_substrings = main_text_matched_substrings;
            }

            public static class MainTextMatchedSubstringsBean {
                /**
                 * length : 3
                 * offset : 0
                 */

                private int length;
                private int offset;

                public int getLength() {
                    return length;
                }

                public void setLength(int length) {
                    this.length = length;
                }

                public int getOffset() {
                    return offset;
                }

                public void setOffset(int offset) {
                    this.offset = offset;
                }
            }
        }

        public static class MatchedSubstringsBean {
            /**
             * length : 3
             * offset : 0
             */

            private int length;
            private int offset;

            public int getLength() {
                return length;
            }

            public void setLength(int length) {
                this.length = length;
            }

            public int getOffset() {
                return offset;
            }

            public void setOffset(int offset) {
                this.offset = offset;
            }
        }

        public static class TermsBean {
            /**
             * offset : 0
             * value : Kukatpally
             */

            private int offset;
            private String value;

            public int getOffset() {
                return offset;
            }

            public void setOffset(int offset) {
                this.offset = offset;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }
        }
    }
}
